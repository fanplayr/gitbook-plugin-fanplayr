# Multi-Purpose Gitbook Plugin

## Installation

Add "fanplayr@git+https://bitbucket.org/fanplayr/gitbook-plugin-fanplayr.git" to
the plugin list.

There are no configuration options.

## Blocks

### tip

```
{% tip %}
Some text
{% endtip %}
```

Creates a "tip" paragraph.

### video

```
{% video source="url" %}
Video caption
{% endvideo %}
```

Embeds a HTML5 video source.
