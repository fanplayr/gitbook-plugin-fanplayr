module.exports = {
  blocks: {
    tip: {
      process: function (block) {
        return this.renderInline('markdown', block.body).then(function (html) {
          var type = block.kwargs.type || 'hint';
          return '<div class="tip tip--' + type + '">' + html + '</div>';
        });
      }
    },
    video: {
      process: function (block) {
        return this.renderInline('markdown', block.body).then(function (html) {
          return '<figure class="screencap">' +
            '<i class="fa fa-play-circle play-icon"></i>' +
            '<video src="' + block.kwargs.source + '" controls preload="metadata"></video>' +
            '<figcaption>Video &mdash; ' + html + '</figcaption>' +
          '</figure>';
        });
      }
    }
  }
};
